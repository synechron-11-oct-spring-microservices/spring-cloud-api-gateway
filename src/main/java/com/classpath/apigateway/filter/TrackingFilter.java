package com.classpath.apigateway.filter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Component
@Slf4j
public class TrackingFilter implements GlobalFilter {
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        log.info("Injecting unique ID for each request :: ");
        if (exchange.getRequest().getHeaders().containsKey("CORRELATION_ID")) {
            log.info(" existing Correlation id , {}", exchange.getRequest().getHeaders().get("CORRELATION_ID"));
            return chain.filter(exchange);
        }else {
            String correlationId = UUID.randomUUID().toString();
            log.info("Generating a correlation id , {}", correlationId);
            exchange.getRequest().mutate().header("CORRELATION_ID", correlationId );
            return chain.filter(exchange);
        }
    }
}